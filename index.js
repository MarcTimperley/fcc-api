const path = require('path')
const express = require('express')
const bootinfo = require(path.join(__dirname, 'utils/bootinfo.js'))
const config = require('./config.js')()
const chalk = require('chalk')
const app = express(config.appName)
const port = process.env.PORT || config.port || 3000
console.log(`${chalk.cyanBright(config.appName)}`)
console.log(bootinfo())
require('./routes')(app)
app.use(express.static(path.join(__dirname, 'public')))
app.use('*', async(req, res, next) => {
  console.log(req.url)
  next()
})
app.get('/', async(req, res, next) => {
  res.send('Hi, and welcome to nodeJS<br/><br/>Check out <a href="./serverinfo/">info</a> about the node process...')
  next()
})

console.log(`Express service is running on port ${chalk.yellowBright(port)}`)
app.listen(port)
