module.exports = function(app) {
  let users = []
  let activities = []
  const bodyParser = require('body-parser')
  app.use(bodyParser.urlencoded({
    extended: false
  }))
  app.post('/api/exercise/new-user', (req, res) => {
    let body = req.body
    console.log(body)
    if (body.name) {
      console.log(body.name)
      if (users.indexOf(body.name) > -1) {
        res.send(JSON.stringify({
          'info': `${body.name} is already registered as #${users.indexOf(body.name)}`
        }))
      } else {
        users.push(body.name)
        console.log(users)
        res.send(JSON.stringify({
          'name': body.name
        }))
      }
    } else {
      res.send(JSON.stringify({
        error: 'Please send parameter name'
      }))
    }
  })
  app.post('/api/exercise/add', (req, res) => {
    let body = req.body
    console.log(body)
    if (body.userId && body.description && body.duration) {
      console.log(body.description)
      if (users[body.userId]) {
        let newActivity = {
          user: body.userId,
          description: body.description,
          duration: body.duration
        }
        if (body.date) newActivity.date = new Date(body.date)
        activities.push(newActivity)
        res.send(JSON.stringify({
          'info': `Added ${newActivity}`
        }))
      } else {
        res.send(JSON.stringify({
          error: 'Please check the userId'
        }))
      }
    } else {
      res.send(JSON.stringify({
        error: 'Please send parameters userId, description and duration (mins)'
      }))
    }
  })
  app.get('/api/exercise/log', (req, res) => {
    let result = JSON.stringify(req.query)
    if (!req.query.userId) {
      result += '<br/>No userId supplied'
    } else {
      if (!users[req.query.userId]) {
        result += '<br/>Cannot find userId ' + req.query.userId
      } else {
        result += '<br/><table><thead><td>description</td><td>duration</td><td>date</td></thead>'
        let activityResults = activities.filter(activity => activity.user === req.query.userId)
        if (req.query.from) activityResults = activityResults.filter(activity => activity.date > new Date(req.query.from))
        if (req.query.to) activityResults = activityResults.filter(activity => activity.date < new Date(req.query.to))
        if (req.query.limit) activityResults = activityResults.slice(0, req.query.limit)
        for (let activity of activityResults) {
          result += `<tr><td>${activity.description || '&nbsp;'}</td><td>${activity.duration || '&nbsp;'}</td><td>${activity.date || '&nbsp;'}</td></tr>`
        }
        result += '</table>'
      }
    }
    res.send(result)
  })
}
