module.exports = function(app) {
  const multer = require('multer')
  const upload = multer({
    dest: './scratch/'
  })
  app.post('/api/fileanalyse/upload', upload.single('upfile'), (req, res) => {
    let result = {
      name: req.file.originalname,
      mimetype: req.file.mimetype,
      size: req.file.size
    }
    res.send(JSON.stringify(result, null, '<br/>'))
  })

  app.get('/api/fileanalyse', (req, res) => {
    let result = '<h2>File Upload</h2>'
    result += '<form method="post" enctype="multipart/form-data" action="/api/fileanalyse/upload"><fieldset>'
    result += '<input id="file" type="file" name="upfile"></input>'
    result += '<button type="submit">Submit</button>'
    result += '<div id="filename"><div>'
    result += '</fieldset></submit>'
    res.send(result)
  })
}
