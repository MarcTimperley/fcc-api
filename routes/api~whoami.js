module.exports = function(app) {
  app.get('/api/whoami/', function(req, res) {
    let userHeaders = req.headers
    let ip = req.headers['x-forwarded-for'] ||
      req.connection.remoteAddress ||
      req.socket.remoteAddress ||
      req.connection.socket.remoteAddress
    console.log(userHeaders)
    res.send(JSON.stringify({
      'ipaddress': ip,
      'language': userHeaders['accept-language'],
      'software': userHeaders['user-agent']
    }))
  })
}
