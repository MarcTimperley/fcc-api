module.exports = function(app) {
  const dns = require('dns')
  const bodyParser = require('body-parser')
  app.use(bodyParser.urlencoded({
    extended: false
  }))
  let urlTable = ['www.google.com', 'news.bbc.co.uk', 'uk.gov', 'https://www.freecodecamp.org/forum/']
  app.get('/api/shorturl/:hash', function(req, res) {
    let hash = req.params.hash
    console.log(hash)
    if (!isNaN(parseInt(hash))) {
      res.redirect(urlTable[hash])
    }
  })
  app.post('/api/shorturl/new/', function(req, res) {
    let input = req.body.url_input
    console.log(input)
    dns.lookup(input, {
      family: 4
    }, (err, address) => {
      if (err) {
        res.send(JSON.stringify({
          error: input + ' is an invalid address',
          errorMessage: err
        }))
      } else {
        console.log(address)
        if (urlTable.indexOf(address) < 0) urlTable.push(address)
        res.send(JSON.stringify({
          // address: address,
          'original address': input,
          'short_url': urlTable.indexOf(address)
        }))
      }
    })
  })
}
