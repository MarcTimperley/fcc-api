module.exports = function(app) {
  app.get('/api/timestamp/:dateString', function(req, res) {
    let dateString = req.params.dateString
    if (!isNaN(dateString)) dateString = parseInt(dateString)
    if (new Date(dateString).getTime()) {
      res.send(JSON.stringify({
        'unix': new Date(dateString).getTime(),
        'utc': new Date(dateString).toUTCString()
      }))
    } else {
      res.send(JSON.stringify({error: 'Invalid Date'}))
    }
  })
  app.get('/api/timestamp/', (req, res) => {
    res.send(JSON.stringify({
      'unix': new Date().getTime(),
      'utc': new Date().toUTCString()
    }))
  })
}
