[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![pipeline status](https://gitlab.com/MarcTimperley/eplate/badges/master/pipeline.svg)](https://gitlab.com/MarcTimperley/eplate/commits/master)
[![coverage report](https://gitlab.com/MarcTimperley/eplate/badges/master/coverage.svg)](https://gitlab.com/MarcTimperley/eplate/commits/master)

# eplate
Express application boilerplate with simple routing structure. Ideally suited to microservices.

##### Why another express boilerplate?
This boilerplate doesn't rely on external libraries apart from [chalk](https://github.com/chalk/chalk) and express itself and allows you to simply add routes without worrying about filling the server index. Configuration is externalised into one file, making it simple to adopt and new routes are just dropped into the routes folder...

# Demo
A demonstration of the application can be found [here](http://www.marctimperley.com/eplate/). This demo is deployed to Heroku using the CI/CD pipeline settings from [Gitlab](https://gitlab.com/MarcTimperley/eplate) on commit.

## Getting Started

  1. Clone the project
  2. Modify the `config.js`
  3. Run `npm install` to get all the dependencies
  4. Run `npm start` to start the server
  5. Add routes in the `routes` folder and their tests in the `__tests__` folder

### Prerequisites

- node 8.x +
- npm 6.x +

### Installing

Follow the getting started steps and access the server on the configured port (http://localhost:3000) unless a different port has been specified in the `config.js` file.

Check the server is responding correctly by visiting the `/`, `/example/` and `/serverinfo/` endpoints.

## Running the tests

To run the tests, jest must be installed (`npm install jest --save-dev` if you don't have it).

- run `npm test` to execute the tests. Shipped tests include validation that routes are loaded and the application has been built correctly.

You are _strongly_ encouraged to write your own tests for any functionality and routes you develop. Check `__tests__/example.test.js` for an example.

You should consider using TDD for the development as it can be very beneficial to reduce the coding effort and ensure the stability of the application itself.

## Contributing and Code of Conduct

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on the code of conduct, and the process for submitting pull requests.

## Authors

* **Marc Timperley** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## ToDo

- [ ] Add css/scss
- [ ] Build all test cases
- [ ] Improve automation
 - [ ] Version management
 - [ ] Test coverage


## Acknowledgments

*
