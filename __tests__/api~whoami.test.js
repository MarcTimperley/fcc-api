const http = require('http')

test('get API response - api~userheaders', done => {
  function callback(data) {
    jest.setTimeout(30000)
    expect(data).toMatch(/"ipaddress"/)
    done()
  }
  http.get('http://localhost:3000/api/whoami/', (resp) => {
    let data = ''
    resp.on('data', (chunk) => {
      data += chunk
    })
    resp.on('end', () => callback(data))
  }).on('error', (err) => {
    console.log('Error: ' + err.message)
  })
})
