const http = require('http')

test('get API response - api~timestamp', done => {
  function callback(data) {
    jest.setTimeout(30000)
    expect(data).toMatch(/unix/)
    done()
  }
  http.get('http://localhost:3000/api/timestamp/', (resp) => {
    let data = ''
    resp.on('data', (chunk) => {
      data += chunk
    })
    resp.on('end', () => callback(data))
  }).on('error', (err) => {
    console.log('Error: ' + err.message)
  })
})

test('get API response - api~timestamp - valid data - string', done => {
  function callback(data) {
    jest.setTimeout(30000)
    expect(data).toMatch(/1546300800000/)
    done()
  }
  http.get('http://localhost:3000/api/timestamp/2019-01-01', (resp) => {
    let data = ''
    resp.on('data', (chunk) => {
      data += chunk
    })
    resp.on('end', () => callback(data))
  }).on('error', (err) => {
    console.log('Error: ' + err.message)
  })
})

test('get API response - api~timestamp - valid data - number', done => {
  function callback(data) {
    jest.setTimeout(30000)
    expect(data).toMatch(/Tue, 01 Jan 2019 00:00:00 GMT/)
    done()
  }
  http.get('http://localhost:3000/api/timestamp/1546300800000', (resp) => {
    let data = ''
    resp.on('data', (chunk) => {
      data += chunk
    })
    resp.on('end', () => callback(data))
  }).on('error', (err) => {
    console.log('Error: ' + err.message)
  })
})

test('get API response - api~timestamp - invalid data', done => {
  function callback(data) {
    jest.setTimeout(30000)
    expect(data).toMatch(/Invalid Date/)
    done()
  }
  http.get('http://localhost:3000/api/timestamp/dummy', (resp) => {
    let data = ''
    resp.on('data', (chunk) => {
      data += chunk
    })
    resp.on('end', () => callback(data))
  }).on('error', (err) => {
    console.log('Error: ' + err.message)
  })
})
