const http = require('http')
const options = {
  method: 'POST',
  hostname: 'localhost',
  port: 3000,
  headers: {
    'Content-Type': 'application/json'
  }
}
test.skip('get API response - api~exercise~new-user - valid data', done => {
  function callback(data) {
    jest.setTimeout(30000)
    expect(data).toMatch(/{"original_url":"}}/)
    done()
  }
  options.path = '/api/exercise/new-user/'
  let postdata = JSON.stringify({
    'name': 'marc'
  })
  options.headers['Content-Length'] = postdata.length
  let req = http.request(options, (resp) => {
    resp.setEncoding('utf8')
    let data = ''
    resp.on('data', (chunk) => {
      data += chunk
      console.log('BODY:' + chunk)
    })
    resp.on('end', () => {
      console.log('no more data')
      callback(data)
    })
  }).on('error', (err) => {
    console.log('Error: ' + err.message)
  })
  req.write(postdata)
  req.end()
})
