const http = require('http')
const options = {
  method: 'POST',
  hostname: 'localhost',
  port: 3000,
  headers: {
    'Content-Type': 'application/json'
  }
}
test.skip('get API response - api~shorturl - valid data - string', done => {
  function callback(data) {
    jest.setTimeout(30000)
    expect(data).toMatch(/{"original_url":"}}/)
    done()
  }
  options.path = '/api/shorturl/new/'
  let postdata = JSON.stringify({
    url_input: 'www.sky.com'
  })
  options.headers['Content-Length'] = postdata.length
  let req = http.request(options, (resp) => {
    resp.setEncoding('utf8')
    let data = ''
    resp.on('data', (chunk) => {
      data += chunk
      console.log('BODY:' + chunk)
    })
    resp.on('end', () => {
      console.log('no more data')
      callback(data)
    })
  }).on('error', (err) => {
    console.log('Error: ' + err.message)
  })
  req.write(postdata)
  req.end()
})

test.skip('get API response - api~shorturl - invalid data', done => {
  function callback(data) {
    jest.setTimeout(30000)
    expect(data).toMatch(/{"error":"invalid URL"}/)
    done()
  }
  options.path = '/api/shorturl/new/blah'
  http.request(options, (resp) => {
    let data = ''
    resp.on('data', (chunk) => {
      data += chunk
    })
    resp.on('end', () => callback(data))
  }).on('error', (err) => {
    console.log('Error: ' + err.message)
  })
})

test('get API response - api~shorturl - existing url', done => {
  function callback(data) {
    jest.setTimeout(30000)
    expect(data).toEqual('Found. Redirecting to uk.gov')
    done()
  }
  options.path = '/api/shorturl/2/'
  options.method = 'GET'
  delete options.headers
  let req = http.get(options, (resp) => {
    let data = ''
    resp.on('data', (chunk) => {
      data += chunk
    })
    resp.on('end', () => callback(data))
  }).on('error', (err) => {
    console.log('Error: ' + err.message)
  })
  req.end()
})
